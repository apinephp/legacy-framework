<?php
/**
 * Collection
 * This file contains the Collection Class
 *
 * @license MIT
 * @copyright 2015-2017 Tommy Teasdale
 */

namespace Apine\Core;

use Apine\Entity\EntityModel;
use Apine\Entity\Overload\EntityModel as OverloadEntityModel;
use Exception;

/**
 * Collection
 * Traversable collection that mimics an array while providing easy to use features
 *
 * @author Tommy Teasdale <tteasdaleroads@gmail.com>
 * @package Apine\Core
 * @deprecated
 */
final class Collection implements \IteratorAggregate
{
    
    /**
     * Object array
     *
     * @var mixed[]
     */
    private $items = [];
    
    /**
     * Add an item to the collection
     *
     * @param mixed  $a_item
     *        Item to add to the collection
     * @param string $a_key
     *        Predefined key of the item into the collection. It is
     *        possible to override existing values, so it is
     *        recommended to not specify a key at the insertion of a
     *        new item.
     *
     * @return mixed
     */
    public function add_item($a_item, $a_key = null)
    {
        // Add the item to the collection
        if ($a_key === null) {
            $this->items[] = $a_item;
        } else {
            $this->items[$a_key] = $a_item;
        }
        
        // Retrieve and return the key
        return array_search($a_item, $this->items, true);
    }
    
    /**
     * Remove an item from the collection
     *
     * @param string $a_key
     *        Key of the item to remove
     *
     * @return boolean
     */
    public function remove_item($a_key)
    {
        if ($this->key_exists($a_key)) {
            unset($this->items[$a_key]);
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Fetch an item from the collection
     * Returns FALSE if the key does not exist
     *
     * @param string $a_key
     *        Key of the item to fetch
     *
     * @return mixed |boolean
     */
    public function get_item($a_key)
    {
        return $this->key_exists($a_key) ? $this->items[$a_key] : false;
    }
    
    /**
     * Fetch all items from the collection
     *
     * @return mixed
     */
    public function get_all()
    {
        return $this->items;
    }
    
    /**
     * Retrieve the first item from the collection
     *
     * @return mixed
     */
    public function get_first()
    {
        return reset($this->items);
    }
    
    /**
     * Retrieve the last item from the collection
     *
     * @return mixed
     */
    public function get_last()
    {
        
        return end($this->items);
    }
    
    /**
     * Add multiple items at once.
     *
     * @param array $a_items
     */
    public function inject_items(array $a_items)
    {
        if (!empty($a_items)) {
            $this->items = array_merge($this->items, $a_items);
        }
    }
    
    /**
     * Sort item from the collection in reverse order
     *
     * @return boolean
     */
    public function reverse()
    {
        try {
            $this->items = array_reverse($this->items);
            
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Sort items from collection by key
     *
     * @return boolean
     */
    public function ksort()
    {
        return ksort($this->items);
    }
    
    /**
     * Get an array of every item keys in the collection
     *
     * @return string[]
     */
    public function keys()
    {
        return ($this->length() !== 0) ? array_keys($this->items) : array();
    }
    
    /**
     * Count all items in the collection
     *
     * @return integer
     */
    public function length()
    {
        return count($this->items);
    }
    
    /**
     * Verify if key exists in the collection
     *
     * @param string $a_key
     *        Key to verify
     *
     * @return boolean
     */
    public function key_exists($a_key)
    {
        return isset($this->items[$a_key]);
    }
    
    /**
     * Verify if value exists in the collection
     * Although it is possible to use this method with
     * EntityModel, the serial validation may give
     * unreliable results depending on your implementation
     * of the entity, thus we do not recommend it.
     *
     * @param mixed $a_value
     *        Value to verify
     *
     * @return boolean
     */
    public function value_exists($a_value)
    {
        try {
            if ($this->length() === 0) {
                return false;
            }
            
            if ($a_value instanceof EntityModel) {
                $a_value->load();
            } else if ($a_value instanceof OverloadEntityModel) {
                $a_value->get_id();
            }
            
            $serial_value = serialize($a_value);
            
            // Cycle through every items in the collection
            foreach ($this->items as $key => $item) {
                if ($item instanceof EntityModel) {
                    $item->load();
                } else if ($item instanceof OverloadEntityModel) {
                    $item->get_id();
                }
    
                if ($serial_value === serialize($item)) {
                    return true;
                }
            }
            
            return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Get Collection iterator (non-PHPdoc)
     *
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator()
    {
        return new CollectionIterator(clone $this);
    }
    
}
